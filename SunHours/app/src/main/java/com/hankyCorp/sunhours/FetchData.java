package com.hankyCorp.sunhours;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class FetchData extends AsyncTask<Void,Void,Void> {

    private final String JSONONBJECT_RESULT = "result";
    private final String JSONOBJECT_SUNRISE = "sunrise";
    private final String JSONOBJECT_SUNSET = "sunset";

    private String longitude,latitude;
    private String data_sun = "";
    private String[] data_sunDate;
    private String[] sunriseByMonths, sunsetByMonths;

    private String sunrise,sunset;


    private final String[] dates = {"2019-01-01","2019-01-15","2019-02-01","2019-02-15","2019-03-01","2019-03-15","2019-04-01","2019-04-15","2019-05-01","2019-05-15","2019-06-01","2019-06-15",
            "2019-07-01","2019-07-15","2019-08-01","2019-08-15","2019-09-01","2019-09-15","2019-10-01","2019-10-15","2019-11-01","2019-11-15","2019-12-01","2019-12-15"};


    public String[] getSunriseByMonths()
    {
        return sunriseByMonths;
    }

    public String[] getSunsetByMonths()
    {
        return sunsetByMonths;
    }

    private WeakReference<Callback> mCallback;

    interface Callback {
        void onPostExecuteCallback();
    }

    public FetchData(Callback callback, LatLng latLng) {
        mCallback = new WeakReference<Callback>(callback);
        sunsetByMonths = new String[dates.length];
        sunriseByMonths = new String[dates.length];
        data_sunDate = new String[dates.length];

        latitude = Double.toString(latLng.latitude);
        longitude = Double.toString(latLng.longitude);
    }

    public String getSunset()
    {
        return sunset;
    }

    public String getSunrise()
    {
        return sunrise;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {


            URL urlSun = new URL("https://api.sunrise-sunset.org/json?lat=" + latitude + "&lng=" + longitude);

            URL[] urlSunDate = new URL[dates.length];

            for(int i = 0; i < dates.length; i++)
            {
                urlSunDate[i] = new URL("https://api.sunrise-sunset.org/json?lat=" + latitude + "&lng=" + longitude + "&date=" + dates[i]);
            }


            JSONObject jsonObjectSun = toJSONObject(urlSun, data_sun);
            JSONObject jsonObjectResult = jsonObjectSun.getJSONObject(JSONONBJECT_RESULT);

            JSONObject[] jsonObjectSunDate = new JSONObject[dates.length];
            JSONObject[] jsonObjectResultSunDate = new JSONObject[dates.length];

            for(int i = 0; i < dates.length; i++)
            {
                jsonObjectSunDate[i] = toJSONObject(urlSunDate[i], data_sunDate[i]);
            }

            for(int i = 0; i < dates.length; i++)
            {
               jsonObjectResultSunDate[i] = jsonObjectSunDate[i].getJSONObject(JSONONBJECT_RESULT);
            }


            for(int i = 0; i < dates.length; i++)
            {
              sunriseByMonths[i] = jsonObjectResultSunDate[i].getString(JSONOBJECT_SUNRISE);
              sunsetByMonths[i] = jsonObjectResultSunDate[i].getString(JSONOBJECT_SUNSET);

            }

            sunrise = jsonObjectResult.getString(JSONOBJECT_SUNRISE);
            sunset = jsonObjectResult.getString(JSONOBJECT_SUNSET);

        }catch (MalformedURLException e)
        {
            e.printStackTrace();
        }catch (IOException e)
        {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private JSONObject toJSONObject(URL url, String data_default) throws IOException, JSONException {

        HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
        InputStream inputStream = httpsURLConnection.getInputStream();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        data_default = "";
        while (line != null)
        {
            line = bufferedReader.readLine();
            data_default = data_default + line;

        }

        JSONObject jsonObject = new JSONObject(data_default);
        return jsonObject;
    }

    @Override
    protected void onPostExecute(Void Void){
        if(mCallback.get() != null){
            mCallback.get().onPostExecuteCallback();
        }
    }
}
