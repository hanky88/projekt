package com.hankyCorp.sunhours;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;


public class MapsActivity extends FragmentActivity implements GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener, OnMapReadyCallback, FetchData.Callback {

    private final int MY_LOCATION_REQUEST_CODE = 2;
    private final double HOURS = 24;
    private final String[] months = {"Januari", "Februari", "Mars","April","Maj","Juni","Juli","Augusti","September","Oktober", "November", "December"};

    private double sunHours;
    private FetchData process;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private LocationManager locationManager;
    private Location location;
    private LatLng latLng;

    private SeekBar seekBar;
    private ProgressBar progressBar, progressBarBackground, progressBar_loading;
    private ToggleButton toggleButton_sunHours,toggleButton_map;

    private TextView textView_monthPeriod, textView_sunset, textView_sunrise, textView_dir_north;

    private String sunrise,sunset;
    private String[] sunriseDate, sunsetDate;
    private double UTCOffset;
    private int monthInProgressIndex;
    private int month;
    private int dayOfMonth;
    private int _progress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.core_activity);

        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);


        seekBar = findViewById(R.id.seekBar);
        toggleButton_sunHours = findViewById(R.id.toggleButton_sunHours);
        toggleButton_map = findViewById(R.id.toggleButton_map);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_LOCATION_REQUEST_CODE);

        }

        progressBar = findViewById(R.id.stats_progressbar);
        progressBarBackground = findViewById(R.id.background_progressbar);
        progressBar_loading = findViewById(R.id.progress_loading);
        textView_monthPeriod = findViewById(R.id.textView_monthPeriod);
        textView_sunrise = findViewById(R.id.textView_sunrise);
        textView_sunset = findViewById(R.id.textView_sunset);
        textView_dir_north = findViewById(R.id.textView_direction_North);

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Date date = new Date();
        UTCOffset = date.getTimezoneOffset() / 60;
        UTCOffset = UTCOffset < 0 ? UTCOffset* -1 : UTCOffset;

        toogleDisableEnableToggleButtons(false);


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(process != null)
                {
                    if(sunriseDate != null && sunsetDate != null)
                    {
                        sunset = sunsetDate[progress];
                        sunrise = sunriseDate[progress];
                        _progress = progress;
                        setMonthPeriodTextview();
                        updateProgress();
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        toggleButton_sunHours.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setSunHoursComponentsVisibility(isChecked);
            }

        });

        toggleButton_map.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                toggleMapTile(isChecked);
            }
        });
    }

    private void toggleMapTile(boolean isChecked) {

        if(isChecked && mMap != null)
        {
            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        }
        else
        {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
        toggleTextviewShadow(isChecked);

    }

    @TargetApi(Build.VERSION_CODES.M)
    private void toggleTextviewShadow(boolean isWhite)
    {

        final int shadowColor;
        if(isWhite)
        {
            shadowColor = R.style.ShadowText_WhiteBlack;

        }
        else
        {
            shadowColor = R.style.ShadowText_BlackWhite;

        }

        textView_sunrise.setTextAppearance(shadowColor);
        textView_sunset.setTextAppearance(shadowColor);
        textView_monthPeriod.setTextAppearance(shadowColor);
        textView_dir_north.setTextAppearance(shadowColor);
    }


    private void updateProgress() {

        if(sunset == null || sunrise == null)
        {
            return;
        }


        double sunsetMinutes = hourToMinutes(sunset,true);

        double sunriseMinutes = hourToMinutes(sunrise,false);

        sunHours = getSunHours(sunriseMinutes,sunsetMinutes);

        int percentage = getSunHoursInPercentage(sunHours);

        progressBar.setProgress(percentage);
        float rot = getProgressRotation(percentage);

        progressBar.setRotation(rot);
        setMonthPeriodTextview();

    }


    private double getSunHours(double sunrise, double sunset)
    {
        double darkHours = (1440 - sunset) + sunrise;
        double _sunHours = 1440 - darkHours;
        return  _sunHours;
    }

    private int getSunHoursInPercentage(double minutes)
    {
        double percentageNoSun = (minutes/60)/HOURS;
        float oneHundred = 100;
        int percentageSun = (int)(oneHundred * percentageNoSun);
        return percentageSun;
    }

    private float getProgressRotation(int sunHoursInPercent)
    {
        float rotation;
        if(sunHoursInPercent == 50)
        {
            rotation = 180;
        }else if( sunHoursInPercent > 50)
        {
            float p = sunHoursInPercent - 50;
            rotation = p * 5.5f;
            rotation = 180 - rotation;

        }else
        {
            float p = 50 - sunHoursInPercent;
            rotation = p * 5.5f;
            rotation = 180 + rotation;
        }

        return rotation;
    }


    private double hourToMinutes(String time, boolean isSunset)
    {
        String _hour = "";
        String _minutes = "";
        int counter = 0;

        for(int i = 0; i < time.length(); i++)
        {

            if(time.charAt(i) == ':')
            {
                counter++;
                continue;
            }

            if(counter == 0)
            {
               _hour = _hour.concat(String.valueOf(time.charAt(i)));
            }else
            {
               _minutes = _minutes.concat(String.valueOf(time.charAt(i)));
            }

            if(_minutes.length() == 2)
                break;

        }


        double hour = Double.valueOf(_hour);
        double minutes = Double.valueOf(_minutes);
        double AmPm = isSunset ? 12 : 0;

        hour += UTCOffset;

        String _RisSetTime = "";

        int hourNoDecimal = (int)hour;
        int amPMNoDecimal = (int)AmPm;
        int minutesNoDecimal = (int)minutes;
        hourNoDecimal += amPMNoDecimal;


        _RisSetTime += hourNoDecimal+ ":" + minutesNoDecimal;

        if(minutesNoDecimal < 10)
            _RisSetTime = new StringBuilder(_RisSetTime).insert(_RisSetTime.length()-1,"0").toString();
        if(hourNoDecimal < 10)
            _RisSetTime = new StringBuilder(_RisSetTime).insert(0,"0").toString();


        if(isSunset)
        {

            textView_sunset.setText("Solnedgång " + _RisSetTime);
        }
        else
        {

            textView_sunrise.setText("Soluppgång " + _RisSetTime);

        }

        double[] hours_minutes ={(hour+AmPm) , minutes};
        return (hours_minutes[0]*60) + minutes;

    }


    private void setSeekBarProgressByThisMonth()
    {
        LocalDate localDate;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            localDate = LocalDate.now();
            month = localDate.getMonthValue();
            dayOfMonth = localDate.getDayOfMonth();
        }
        else
        {
            Calendar calendar = Calendar.getInstance();
            month = calendar.get(Calendar.MONTH) + 1;
            dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        }

         monthInProgressIndex = (month*2) - 1;

        if(dayOfMonth < 8)
        {
            monthInProgressIndex -= 1;
        }

        seekBar.setProgress(monthInProgressIndex);

    }

    private void setSunHoursComponentsVisibility(boolean isInvisible)
    {
        if(isInvisible)
        {

            seekBar.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
            progressBarBackground.setVisibility(View.INVISIBLE);
            textView_monthPeriod.setVisibility(View.INVISIBLE);
            textView_sunset.setVisibility(View.INVISIBLE);
            textView_sunrise.setVisibility(View.INVISIBLE);
            textView_dir_north.setVisibility(View.INVISIBLE);
        }
        else
        {
            seekBar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            progressBarBackground.setVisibility(View.VISIBLE);
            textView_monthPeriod.setVisibility(View.VISIBLE);
            textView_sunset.setVisibility(View.VISIBLE);
            textView_sunrise.setVisibility(View.VISIBLE);
            textView_dir_north.setVisibility(View.INVISIBLE);

        }

    }

    private void toogleDisableEnableToggleButtons(boolean isEnabled)
    {
        toggleButton_map.setEnabled(isEnabled);
        toggleButton_sunHours.setEnabled(isEnabled);
    }


    private void setMonthPeriodTextview() {

        String period;

        if(_progress%2 == 0)
        {
            period = "Början av ";
        }
        else
        {
            period = "Mitten av ";
        }
        int adjuster = (_progress +1)%2;
        int progress = (_progress + 1)/2;
        progress += adjuster;
        period += months[progress - 1];
        textView_monthPeriod.setText(period);
    }

    private void addMarker()
    {
        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_LOCATION_REQUEST_CODE);


                return;
            }
        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        mMap.getUiSettings().setRotateGesturesEnabled(false);

        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        latLng = new LatLng(location.getLatitude(),location.getLongitude());
        process = new FetchData(this,latLng);


        if(process.getStatus() == AsyncTask.Status.PENDING)
            process.execute();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_LOCATION_REQUEST_CODE) {
            mapFragment.getMapAsync(this);
            if (permissions.length == 1 && permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {


            } else {
                Toast.makeText(this, "Set permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setSunHoursComponentsVisibility(true);
        setSunHoursComponentsVisibility(false);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){

        }
    }

    @Override
    public void onPostExecuteCallback() {
        progressBar_loading.setVisibility(View.GONE);
        sunrise = process.getSunrise();
        sunset = process.getSunset();

        sunriseDate = process.getSunriseByMonths();
        sunsetDate = process.getSunsetByMonths();

        setSunHoursComponentsVisibility(false);
        toogleDisableEnableToggleButtons(true);
        setSeekBarProgressByThisMonth();
        setMonthPeriodTextview();
        updateProgress();
        addMarker();

    }

}
